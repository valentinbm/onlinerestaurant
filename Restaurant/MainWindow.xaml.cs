﻿using Restaurant.ViewModels;
using Restaurant.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Restaurant
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private ClientSignInView clientSignInView = new ClientSignInView();
        private UserLogInView userLogInView = new UserLogInView();

        public MenuPresentationView MenuPresentationControl { get; } = new MenuPresentationView();
        public CartView CartControl { get; } = new CartView();
        public MainWindow()
        {
            InitializeComponent();
            CartControl.DataContext = new CartViewModel
                ((MenuPresentationControl.DataContext as MenuPresentationViewModel).ProductAddedPieces);
        }

        private void CreateAccout_Click(object sender, RoutedEventArgs e)
        {
            this.contentControl.Content = clientSignInView;
        }

        private void Login_Click(object sender, RoutedEventArgs e)
        {
            this.contentControl.Content = userLogInView;
        }

        private void ViewMenu_Click(object sender, RoutedEventArgs e)
        {
            this.contentControl.Content = MenuPresentationControl;
        }

        private void ViewCart_Click(object sender, RoutedEventArgs e)
        {
            this.contentControl.Content = CartControl;
        }
    }
}
