﻿using Restaurant.Models.DataAccessLayer;
using Restaurant.Models.EntityLayer;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Restaurant.Models.BusinessLogicLayer
{
    class CategoryBLL
    {
        private CategoryDAL categoryDAL = new CategoryDAL();

        public ObservableCollection<Category> GetAllCategories()
        {
            return categoryDAL.GetAllCategories();
        }
    }
}
