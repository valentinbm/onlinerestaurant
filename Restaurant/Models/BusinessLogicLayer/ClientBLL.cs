﻿using Restaurant.Models.DataAccessLayer;
using Restaurant.Models.EntityLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Restaurant.Models.BusinessLogicLayer
{
    class ClientBLL
    {
        ClientDAL clientDAL = new ClientDAL();
        UserDAL userDAL = new UserDAL();

        public void AddNewClient(User user, Client client)
        {

            int? userId = userDAL.AddUser(user);

            if (userId != null)
            {
                clientDAL.AddClient(client, (int)userId);
                return;
            }
        }

        public Client GetClient(int userId)
        {
            return clientDAL.GetClient(userId);
        }
    }
}
