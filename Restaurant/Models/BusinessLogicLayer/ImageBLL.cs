﻿using Restaurant.Models.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Restaurant.Models.BusinessLogicLayer
{
    class ImageBLL
    {
        private ImageDAL imageDal = new ImageDAL();

        private string SaveImage(int imgId, string imgFolderPath)
        {
            string imgName = imageDal.GetImageName(imgId);
           
            imageDal.SaveImage(imgId, imgName, imgFolderPath);

            return imgName;
        }

        public string GetImageUrl(int imgId)
        {
            string projectDirectory = Directory.GetParent(Environment.CurrentDirectory).Parent.FullName;

            string imgFolderPath = projectDirectory + @"\Resources\Images";
            
            string imgName = SaveImage(imgId, imgFolderPath);

            string imgUrl = imgFolderPath + "\\" + imgName;

            return imgUrl;
        }
    }
}
