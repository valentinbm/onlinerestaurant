﻿using Restaurant.Models.DataAccessLayer;
using Restaurant.Models.EntityLayer;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Restaurant.Models.BusinessLogicLayer
{
    class ProductBLL
    {
        private ProductDAL productDAL = new ProductDAL();

        public ObservableCollection<Product> GetAllProducts()
        {
            return productDAL.GetAllProducts();
        }
    }
}
