﻿using Restaurant.Models.DataAccessLayer;
using Restaurant.Models.EntityLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Restaurant.Models.BusinessLogicLayer
{
    class UserBLL
    {
        public bool VerifyUserCredentials(User user)
        {
            UserDAL userDAL = new UserDAL();
            int? id = userDAL.VerifyUserCredentials(user);

            if ( id != null)
            {
                user.UserId = (int)id;
                return true;
            }

            return false;
        }
    }
}
