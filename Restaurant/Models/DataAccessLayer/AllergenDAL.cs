﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Restaurant.Models.DataAccessLayer
{
    class AllergenDAL
    {
        internal List<string> GetAllergensForProduct(int productId)
        {
            using (SqlConnection con = DALHelper.Connection)
            {
                SqlCommand cmdAllergens = new SqlCommand("spGetAllergensForProduct", con);
                List<string> allergens = new List<string>();
                cmdAllergens.CommandType = CommandType.StoredProcedure;
                cmdAllergens.Parameters.AddWithValue("@ProductId", productId);
                con.Open();
                SqlDataReader readerAllergens = cmdAllergens.ExecuteReader();

                while (readerAllergens.Read())
                {
                    allergens.Add(readerAllergens.GetString(0));
                }

                readerAllergens.Close();
                return allergens;
            }
        }
    }
}
