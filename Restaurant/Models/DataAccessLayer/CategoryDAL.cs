﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Restaurant.Models.DataAccessLayer
{
    class CategoryDAL
    {
        internal ObservableCollection<EntityLayer.Category> GetAllCategories()
        {
            using (SqlConnection con = DALHelper.Connection)
            {
                SqlCommand cmd = new SqlCommand("spGetAllCategories", con);
                ObservableCollection<EntityLayer.Category> result = new ObservableCollection<EntityLayer.Category>();
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    EntityLayer.Category category = new EntityLayer.Category();

                    category.CategoryId = reader.GetInt32(0);
                    category.Name = reader.GetString(1);
                    category.IsActive = reader.GetBoolean(2);

                    result.Add(category);
                }

                reader.Close();
                return result;
            }
        }
    }
}
