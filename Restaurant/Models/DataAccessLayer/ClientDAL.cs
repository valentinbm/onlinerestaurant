﻿using Restaurant.Models.EntityLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Restaurant.Models.DataAccessLayer
{
    class ClientDAL
    {
        internal void AddClient(Client client, int userId)
        {
            using (SqlConnection con = DALHelper.Connection)
            {
                SqlCommand cmd = new SqlCommand("spAddClient", con);
                cmd.CommandType = CommandType.StoredProcedure;
                string responseMessage = String.Empty;
                cmd.Parameters.AddWithValue("@FirstName", client.FirstName);
                cmd.Parameters.AddWithValue("@LastName", client.LastName);
                cmd.Parameters.AddWithValue("@UserId", userId);
                cmd.Parameters.AddWithValue("@Phone", client.Phone);
                cmd.Parameters.AddWithValue("@DeliveryAddress", client.DeliveryAddress);
                cmd.Parameters.AddWithValue("@responseMessage", responseMessage);
                con.Open();
                cmd.ExecuteNonQuery();
            }
        }

        internal Client GetClient(int userId)
        {
            using (SqlConnection con = DALHelper.Connection)
            {
                Client client = new Client();
                SqlCommand cmd = new SqlCommand("spGetClientWithUserId", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@UserId", userId);
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                reader.Read();

                client.UserId = userId;
                client.ClientId = reader.GetInt32(0);
                client.FirstName = reader.GetString(1);
                client.LastName = reader.GetString(2);
                client.DeliveryAddress = reader.GetString(3);
                client.Phone = reader.GetString(4);

                return client;
            }
        }

    }
}
