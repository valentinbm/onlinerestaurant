﻿using Restaurant.Properties;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Restaurant.Models.DataAccessLayer
{
    class ImageDAL
    {
        internal string GetImageName(int imageId)
        {
            using (SqlConnection con = DALHelper.Connection)
            {
                SqlCommand cmd = new SqlCommand("spGetImage", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ImageId", imageId);
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                reader.Read();

                return reader.GetString(0);
            }
        }

        internal void SaveImage(int imageId, string imageName, string imageFolderPath)
        {
            using (SqlConnection con = DALHelper.Connection)
            {
                SqlCommand cmd = new SqlCommand("spExportImage", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ImageId", imageId);
                cmd.Parameters.AddWithValue("@ImageFolderPath", imageFolderPath);
                cmd.Parameters.AddWithValue("@ImageName", imageName);
                con.Open();
                cmd.ExecuteNonQuery();
            }
        }
    }
}
