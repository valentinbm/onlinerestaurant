﻿using Restaurant.Models.EntityLayer;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Restaurant.Models.DataAccessLayer
{
    class ProductDAL
    {
        internal ObservableCollection<Product> GetAllProducts()
        {
            using (SqlConnection con = DALHelper.Connection)
            {
                SqlCommand cmdProducts = new SqlCommand("spGetAllProducts", con);
                ObservableCollection<Product> products = new ObservableCollection<Product>();
                cmdProducts.CommandType = CommandType.StoredProcedure;
                con.Open();
                SqlDataReader readerProducts = cmdProducts.ExecuteReader();

                while (readerProducts.Read())
                {
                    Product product = new Product();

                    product.ProductId = readerProducts.GetInt32(0);
                    product.CategoryId = readerProducts.GetInt32(1);
                    product.ImageId = readerProducts.GetInt32(2);
                    product.Name = readerProducts.GetString(3);
                    product.Price = readerProducts.GetDecimal(4);
                    product.PortionQuantity = readerProducts.GetDecimal(5);
                    product.TotalQuantity = readerProducts.GetDecimal(6);
                    product.IsActive = readerProducts.GetBoolean(7);

                    
                    products.Add(product);
                }

                readerProducts.Close();
                return products;
            }
        }
    }
}
