﻿using Prism.Logging;
using Restaurant.Models.EntityLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Restaurant.Models.DataAccessLayer
{
    class UserDAL
    {
        internal int? AddUser(User user)
        {
            using (SqlConnection con = DALHelper.Connection)
            {
                string responseMessage = String.Empty;
                SqlCommand cmd = new SqlCommand("spAddUser", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Email", user.Email);
                cmd.Parameters.AddWithValue("@Password", user.Password);
                SqlParameter returnValue = new SqlParameter("@responseMessage", SqlDbType.NVarChar, 250);
                returnValue.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(returnValue);
                con.Open();

                int? insertedId = (int?)cmd.ExecuteScalar();

                if (insertedId == null)
                {
                    throw new Exception((string)cmd.Parameters["@responseMessage"].Value);
                }

                return insertedId;
            }
        }

        internal int? VerifyUserCredentials(User user)
        {
            using (SqlConnection con = DALHelper.Connection)
            {
                string responseMessage = String.Empty;
                SqlCommand cmd = new SqlCommand("spVerifyUserCredentials", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Email", user.Email);
                cmd.Parameters.AddWithValue("@Password", user.Password);
                SqlParameter returnValue = new SqlParameter("@id", SqlDbType.Int);
                returnValue.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(returnValue);
                con.Open();
                cmd.ExecuteNonQuery();
                object result = cmd.Parameters["@id"].Value;
                try
                {
                    return (int?)result;
                }
                
                catch(InvalidCastException)
                {
                    return null;
                }
            }
        }
    }
}
