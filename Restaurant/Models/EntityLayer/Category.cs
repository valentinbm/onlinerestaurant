﻿using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Restaurant.Models.EntityLayer
{
    class Category : BindableBase
    {
        private int categoryId;
        private string name;
        private bool isActive;
        public int CategoryId
        {
            get
            {
                return categoryId;
            }
            set
            {
                SetProperty(ref categoryId, value, "CategoryId");
            }
        }

        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                SetProperty(ref name, value, "Name");
            }
        }

        public bool IsActive
        {
            get
            {
                return isActive;
            }
            set
            {
                SetProperty(ref isActive, value, "IsActive");
            }
        }

        public override string ToString()
        {
            return name;
        }
    }
}
