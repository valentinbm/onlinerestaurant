﻿using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Restaurant.Models.EntityLayer
{
    class Client : BindableBase
    {
        private int clientId;
        private string firstName;
        private string lastName;
        private int userId;
        private string deliveryAddress;
        private string phone;
        public string FirstName
        {
            get
            {
                return firstName;
            }
            set
            {
                SetProperty(ref firstName, value, "FirstName");
            }
        }

        public string LastName
        {
            get
            {
                return lastName;
            }
            set
            {
                SetProperty(ref lastName, value, "LastName");
            }
        }

        public int UserId
        {
            get
            {
                return userId;
            }
            set
            {
                SetProperty(ref userId, value, "UserId");
            }
        }

        public string DeliveryAddress
        {
            get
            {
                return deliveryAddress;
            }
            set
            {
                SetProperty(ref deliveryAddress, value, "DeliveryAddress");
            }
        }

        public string Phone
        {
            get
            {
                return phone;
            }
            set
            {
                SetProperty(ref phone, value, "Phone");
            }
        }

        public int ClientId
        {
            get
            {
                return clientId;
            }
            set
            {
                SetProperty(ref clientId, value, "ClientId");
            }
        }
    }
}
