﻿using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Restaurant.Models.EntityLayer
{
    class Order : BindableBase
    {
        int orderId;
        DateTime orderDate;
        string status;
        double price;
        List<Product> products = new List<Product>();

        public int OrderId
        {
            get
            {
                return orderId;
            }
            set
            {
                SetProperty(ref orderId, value, "OrderId");
            }
        }

        public DateTime OrderDate
        {
            get
            {
                return orderDate;
            }
            set
            {
                SetProperty(ref orderDate, value, "OrderDate");

            }
        }

        public double Price
        {
            get
            {
                return price;
            }
            set
            {
                SetProperty(ref price, value, "Price");
            }
        }

        public string Status
        {
            get
            {
                return status;
            }
            set
            {
                SetProperty(ref status, value, "Status");
            }
        }
        public List<Product> Products
        {
            get
            {
                return products;
            }
            set
            {
                SetProperty(ref products, value, "Products");
            }
        }
        
    }
}
