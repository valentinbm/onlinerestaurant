﻿using Prism.Mvvm;
using Restaurant.Models.BusinessLogicLayer;
using Restaurant.Models.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Restaurant.Models.EntityLayer
{
    class Product : BindableBase
    {
        private int productId;
        private int imageId;
        private int categoryId;
        private string name;
        private decimal price;
        private decimal portionQuantity;
        private decimal totalQuantity;
        private bool isActive;

        private List<string> allergens;
        private string imagePath;

        public int ProductId
        {
            get
            {
                return productId;
            }
            set
            {
                if (SetProperty(ref productId, value, "ProductId"))
                {
                    allergens = new AllergenDAL().GetAllergensForProduct(productId);
                }
            }
        }

        public int CategoryId
        {
            get
            {
                return categoryId;
            }
            set
            {
                SetProperty(ref categoryId, value, "CategoryId");
            }
        }

        public int ImageId
        {
            get
            {
                return imageId;
            }
            set
            {
                if (SetProperty(ref imageId, value, "ImageId"))
                {
                    imagePath = new ImageBLL().GetImageUrl(imageId);
                }
            }
        }

        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                SetProperty(ref name, value, "Name");
            }
        }


        public decimal Price
        {
            get
            {
                return price;
            }
            set
            {
                SetProperty(ref price, value, "Price");
            }
        }



        public decimal PortionQuantity
        {
            get
            {
                return portionQuantity;
            }
            set
            {
                SetProperty(ref portionQuantity, value, "PortionQuantity");
            }
        }



        public decimal TotalQuantity
        {
            get
            {
                return totalQuantity;
            }
            set
            {
                SetProperty(ref totalQuantity, value, "TotalQuantity");
            }
        }

        public bool IsActive
        {
            get
            {
                return isActive;
            }
            set
            {
                SetProperty(ref isActive, value, "IsActive");
            }
        }
        public List<string> Allergens
        {
            get
            {
                return allergens;
            }
            set
            {
                SetProperty(ref allergens, value, "Allergens");
            }
        }

        public string ImagePath
        {
            get
            {
                return imagePath;
            }
            set
            {
                SetProperty(ref imagePath, value, "imagePath");
            }
        }
    }
}
