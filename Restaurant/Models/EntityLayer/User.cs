﻿using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Restaurant.Models.EntityLayer
{
    class User : BindableBase
    {
        private int userId;
        private string email;
        private string password;

        public string Email
        {
            get
            {
                return email;
            }
            set
            {
                SetProperty(ref email, value, "Email");
            }
        }

        public string Password
        {
            get
            {
                return password;
            }
            set
            {
                SetProperty(ref password, value, "Password");
            }
        }

        public int UserId
        {
            get
            {
                return userId;
            }
            set
            {
                SetProperty(ref userId, value, "UserId");
            }
        }
    }
}
