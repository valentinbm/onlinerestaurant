﻿using Prism.Mvvm;
using Restaurant.Commands;
using Restaurant.Models.EntityLayer;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Restaurant.ViewModels
{
    class CartViewModel : BindableBase
    {
        private ObservableCollection<Tuple<Product, int>> productAddedPieces;
        private Tuple<Product, int> selectedProductPieces;
        private RelayCommand pressDelete;
        private RelayCommand pressIncrease;
        private RelayCommand pressDecrease;
        private Client client = null;

        public CartViewModel(ObservableCollection<Tuple<Product, int>> productAddedPieces)
        {
            this.productAddedPieces = productAddedPieces;
        }

        public ObservableCollection<Tuple<Product, int>> ProductAddedPieces
        {
            get
            {
                return productAddedPieces;
            }

            set
            {
                SetProperty(ref productAddedPieces, value, "ProductAddedPieces");
            }
        }

        public Client CurrentClient
        {
            get
            {
                return client;
            }
            set
            {
                SetProperty(ref client, value, "CurrentClient");
            }
        }

        public Tuple<Product, int> SelectedProductPieces
        {
            get
            {
                return selectedProductPieces;
            }
            set
            {
                SetProperty(ref selectedProductPieces, value, "SelectedProductPieces");
            }
        }

        private void increaseNumberOfPieces(object param)
        {
            if (selectedProductPieces != null)
            {
                productAddedPieces.Add(new Tuple<Product, int>(selectedProductPieces.Item1, selectedProductPieces.Item2 + 1));
                productAddedPieces.Remove(selectedProductPieces);
            }
        }


        public RelayCommand PressIncrease
        {
            get
            {
                pressIncrease = new RelayCommand(increaseNumberOfPieces, param => true);
                return pressIncrease;
            }

        }

        private void decreaseNumberOfPieces(object param)
        {
            if (selectedProductPieces != null)
            {
                if (selectedProductPieces.Item2 - 1 < 1)
                {
                    productAddedPieces.Remove(selectedProductPieces);
                    return;
                }

                productAddedPieces.Add(new Tuple<Product, int>(selectedProductPieces.Item1, selectedProductPieces.Item2 - 1));
                productAddedPieces.Remove(selectedProductPieces);
            }
        }

        public RelayCommand PressDecrease
        {
            get
            {
                pressDecrease = new RelayCommand(decreaseNumberOfPieces, param => true);
                return pressDecrease;
            }

        }

        private void removeProduct(object param)
        {
            if (selectedProductPieces != null)
            {
                productAddedPieces.Remove(selectedProductPieces);
            }
        }



        public RelayCommand PressDelete
        {
            get
            {
                pressDelete = new RelayCommand(removeProduct, param => true);
                return pressDelete;
            }

        }


        //private SimpleRelayCommand pressAddCommand;

        //public SimpleRelayCommand PressAddCommand
        //{
        //    get
        //    {
        //        pressAddCommand = new SimpleRelayCommand(cartBLL.AddCommand, param => true);
        //        return pressAddCommand;
        //    }

        //}
    }
}
