﻿using Prism.Mvvm;
using Restaurant.Commands;
using Restaurant.Models.BusinessLogicLayer;
using Restaurant.Models.EntityLayer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Restaurant.ViewModels 
{
    class ClientSignInViewModel : BindableBase
    {
        private ClientBLL clientBLL = new ClientBLL();

        private string registerMessage = String.Empty;
        public Client NewClient { get; } = new Client();

        public User NewUser { get; } = new User();

        private void addNewClient(object param)
        {
            if(String.IsNullOrEmpty(NewClient.FirstName) || String.IsNullOrEmpty(NewClient.LastName) 
                || String.IsNullOrEmpty(NewClient.DeliveryAddress) || String.IsNullOrEmpty(NewClient.Phone)
                || String.IsNullOrEmpty(NewUser.Email) || String.IsNullOrEmpty(NewUser.Password))
            {
                RegisterMessage = "Error : All fiels must be completed!";
                return;
            }

            try
            {
                clientBLL.AddNewClient(NewUser, NewClient);
                RegisterMessage = "Client account registered succesfully! Now you can login to make orders!";
            }
            catch(Exception e)
            {
                RegisterMessage = e.Message;
            }
        }

        public string RegisterMessage
        {
            get
            {
                return registerMessage;
            }
            set
            {
                SetProperty(ref registerMessage, value, "RegisterMessage");
            }
        }

        private RelayCommand save;

        public RelayCommand Save
        {
            get
            {

                save = new RelayCommand(addNewClient, param => true);

                return save;
            }
        }
    }
}
