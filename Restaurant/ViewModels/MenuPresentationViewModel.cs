﻿using Prism.Mvvm;
using Restaurant.Models.EntityLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using Restaurant.Models.BusinessLogicLayer;
using Restaurant.Models.DataAccessLayer;
using Restaurant.Commands;

namespace Restaurant.ViewModels
{
    class MenuPresentationViewModel : BindableBase
    {
        private CategoryBLL categoryBLL = new CategoryBLL();
        private ObservableCollection<Category> categories;

        private ProductBLL productBLL = new ProductBLL();
        private ObservableCollection<Product> products;

        private ObservableCollection<Tuple<Product, int>> productAddedPieces = new ObservableCollection<Tuple<Product, int>>();

        public ObservableCollection<Tuple<Product, int>> ProductAddedPieces
        {
            get
            {
                return productAddedPieces;
            }

            set
            {
                SetProperty(ref productAddedPieces, value, "ProductAddedPieces");
            }
        }

        public void AddProduct(Product product)
        {
            foreach(var t in productAddedPieces)
            {
                if(t.Item1.ProductId == product.ProductId)
                {
                    productAddedPieces.Add(new Tuple<Product, int>(product, t.Item2 + 1));
                    productAddedPieces.Remove(t);
                    return;
                }
            }
            productAddedPieces.Add(new Tuple<Product, int>(product, 1));
        }

        public ObservableCollection<Category> Categories
        {
            get
            {
                return categories;
            }

            set
            {
                SetProperty(ref categories, value, "Categories");
            }
        }

        public ObservableCollection<Product> Products
        {
            get
            {
                return products;
            }

            set
            {
                SetProperty(ref products, value, "Products");
            }
        }

        public ObservableCollection<Product> GetProductsFromCategory(int categoryId)
        {
            ObservableCollection<Product> prods = new ObservableCollection<Product>();

            foreach(Product prod in products)
            {
                if(prod.CategoryId == categoryId)
                {
                    prods.Add(prod);
                }
            }

            return prods;
        }

        public MenuPresentationViewModel()
        {
            categories = categoryBLL.GetAllCategories();
            products = productBLL.GetAllProducts();


        }
    }
}
