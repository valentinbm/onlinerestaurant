﻿using Prism.Mvvm;
using Restaurant.Commands;
using Restaurant.Models.BusinessLogicLayer;
using Restaurant.Models.EntityLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Restaurant.ViewModels
{
    class UserLogInViewModel : BindableBase
    {
        private UserBLL userBLL = new UserBLL();

        private string loginMessage = String.Empty;
        public User LogInUser { get; } = new User();

        private Client client = null;

        public Client CurrentClient
        {
            get
            {
                return client;
            }
            set
            {
                SetProperty(ref client, value, "CurrentClient");
            }
        }

        public string LoginMessage
        {
            get
            {
                return loginMessage;
            }
            set
            {
                SetProperty(ref loginMessage, value, "LoginMessage");
            }
        }
        public bool ExecuteLogInUser()
        {
            if (String.IsNullOrEmpty(LogInUser.Email) || String.IsNullOrEmpty(LogInUser.Password))
            {
                LoginMessage = "All fields must me completed!";
                return false;
            }

            if(!userBLL.VerifyUserCredentials(LogInUser))
            {
                LoginMessage = "The email or password is incorrect!";
                return false;
            }

            LoginMessage = "Login succeded! Now you can make orders from the menu!";
            CurrentClient = new ClientBLL().GetClient(LogInUser.UserId);
            return true;
        }
        //public RelayCommand LogIn
        //{
        //    get
        //    {

        //        logIn = new RelayCommand(logInUser, param => true);

        //        return logIn;
        //    }
        //}
    }
}
