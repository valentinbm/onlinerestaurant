﻿using Prism.Logging;
using Restaurant.Models.EntityLayer;
using Restaurant.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Restaurant.Views
{
    /// <summary>
    /// Interaction logic for ProductsPresentationView.xaml
    /// </summary>
    public partial class MenuPresentationView : System.Windows.Controls.UserControl
    {
        public MenuPresentationView()
        {
            InitializeComponent();
            DataContext = new MenuPresentationViewModel();
            cmbCategories.ItemsSource = (DataContext as MenuPresentationViewModel).Categories;
            btnAddToCart.Visibility = Visibility.Hidden;
        }

        private void cmbCategories_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if(cmbCategories.SelectedItem != null)
            {
                Models.EntityLayer.Category selectedCategory = (Models.EntityLayer.Category) cmbCategories.SelectedItem;
                lbProducts.ItemsSource = (DataContext as MenuPresentationViewModel).GetProductsFromCategory(selectedCategory.CategoryId);
                return;
            }

            lbProducts.ItemsSource = (DataContext as MenuPresentationViewModel).Products;
        }

        private void btnAddToCart_Click(object sender, RoutedEventArgs e)
        {
            if (lbProducts.SelectedItem != null)
            {
                Product selectedProduct = (Product)lbProducts.SelectedItem;
                MenuPresentationViewModel menuPresentationViewModel = DataContext as MenuPresentationViewModel;
                menuPresentationViewModel.AddProduct(selectedProduct);
            }
        }
     
    }
}
