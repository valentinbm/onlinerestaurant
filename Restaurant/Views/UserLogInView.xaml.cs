﻿using Restaurant.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Restaurant.Views
{
    /// <summary>
    /// Interaction logic for UserSignInView.xaml
    /// </summary>
    public partial class UserLogInView : UserControl
    {
        public UserLogInView()
        {
            InitializeComponent();
            DataContext = new UserLogInViewModel();
        }

        private void btnLogIn_Click(object sender, RoutedEventArgs e)
        {
            UserLogInViewModel userLogInViewModel = DataContext as UserLogInViewModel;

            if(userLogInViewModel.ExecuteLogInUser())
            {
                MainWindow mainWindow = Window.GetWindow(this) as MainWindow;
                (mainWindow.CartControl.DataContext as CartViewModel).CurrentClient = userLogInViewModel.CurrentClient;
                mainWindow.MenuPresentationControl.btnAddToCart.Visibility = Visibility.Visible;
                mainWindow.contentControl.Content = mainWindow.MenuPresentationControl;
            }
        }
    }
}
